package com.example.ciberseguridad_ud3;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    //fingerprints
    private static final String CERTIFICATE_OK = "BE3C4113CFF7F92A2D862C09FD5DA1EF31AD3F54";
    private static final String CERTIFICATE_KO = "5755A6D57BF84C19756C9D0747152B574718AD47";

    private TextView textViewValidatedStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewValidatedStatus = (TextView) findViewById(R.id.textViewValidatedStatus);
    }

    public void validateCertificateOK(View view) {
        if (validateAppSignature(this, CERTIFICATE_OK)) {
            textViewValidatedStatus.setText("Certificado OK!!!");
        } else {
            textViewValidatedStatus.setText("Certificado KO!!!");
        }
    }

    public void validateCertificateKO(View view) {
        if (validateAppSignature(this, CERTIFICATE_KO)) {
            textViewValidatedStatus.setText("Certificado OK!!!");
        } else {
            textViewValidatedStatus.setText("Certificado KO!!!");
        }
    }


    //sacamos el fingerprint de la app
    private static boolean validateAppSignature(Context context, String sha1) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES
            );
            //sacamos el signature
            Signature[] appsSignatures = packageInfo.signatures;
            for (Signature signature : appsSignatures) {
                byte[] singnatureBytes = signature.toByteArray();
                String currentSignature = calcSHA1(singnatureBytes);
                //lo comparamos con el que nos llega
                return sha1.equalsIgnoreCase(currentSignature);
            }
        } catch (Exception e) {
            Log.i("validateAppSignature", e.toString());
        }
        return false;
    }

    private static String calcSHA1(byte[] signature) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA1");
        digest.update(signature);
        byte[] signatureHash = digest.digest();
        return bytesToHex(signatureHash);
    }

    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
